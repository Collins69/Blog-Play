package models;

import java.util.HashSet;
import java.util.Set;

import com.avaje.ebean.Models;


import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Wandji Collins on 09/10/2018
 */

 @Entity

public class BookExtendsModel{

    @Id

    public Integer id;
    public String title;
    public Integer price;
    public String author;

    public static Finder<Integer, Book> find = new Finder<>(Book.class);

}

//    public Book (){
//
//    }
//
//    public Book (Integer id ,  String title, Integer price, String author){
//        this.Id = id;
//        this.title = title;
//        this.price = price;
//        this.author = author;
//    }
//
//    private static Set<Books> books;
//
//    static {
//        books = new HashSet<books>();
//        books.add(new Book (id : 1, title: "Python", price: "$20" , author: "thonis"));
//        books.add(new Book (id : 2, title: "C++", price: "$30", author: "Deitel"));
//        books.add(new Book (id : 3, title: "Java", price: "$50", author "ABD"));
//
//        }
//
//        public static Set<Book> allbooks(){
//
//            return books;
//        }
//
//        public static Book findById(Integer Id){
//
//        for (Book book : books){
//
//            if (Id.equals(book.Id)){
//                return book;
//            }
//        }
//
//        }
//


